<div class="collapse navbar-collapse pull-left" id="navbar-collapse">
  <ul class="nav navbar-nav">
    <li class="{{ $url == 'search' ? 'active' : '' }}">
        <a href="{{action('SearchController@index')}}">ค้นหา</a></li>
    <li class="{{ $url == 'recive' ? 'active' : '' }}">
      <a href="{{action('ReciveController@index')}}">ลงรับ</a></li>
    <li class="{{ $url == 'send' ? 'active' : '' }}">
      <a href="{{action('SendController@index')}}">ลงส่ง</a></li>
  </ul>
  <!-- <form class="navbar-form navbar-left" role="search">
    <div class="form-group">
      <input type="text" class="form-control" id="navbar-search-input" placeholder="Search">
    </div>
  </form> -->
</div>
