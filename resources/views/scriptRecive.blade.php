<script>
var url = "{{action('ReciveController@store')}}";
var table = $('#datatable').DataTable({
  searching:false,
  "fnCreatedRow": function (nRow, aData, iDataIndex) {
    $(nRow).attr('id', aData["id"]);
  },
  "columns": [
    { "title": "เลขทะเบียนรับ","data": "doc" },
    { "title": "ที่","data": "doc_no" },
    { "title": "ลงวันที่","data": "datefrom" },
    { "title": "จาก","data": "from" },
    { "title": "ถึง","data": "to" },
    { "title": "เรื่อง","data": "subject" },
    { "title": "งบประมาณ","data": "budget" },
    { "title": "รับเรื่อง","data": "recipence" },
    { "title": "ส่งแล้ว","data": "sendto" },
    { "title": "ส่งวันที่","data": "senddate" },
    { "title": "หมายเหตุ","data": "note" },
    { "title": "#","data": "button" },
    { "title": "วันที่เข้า","data": "created_at" },
    { "title": "วันที่อัพเดท","data": "updated_at" }
  ]
});
$(document).ready(function(){

  $('input').keypress(function(event){

    if(event.which=='13'){
      add_data();
    }
  });
  $('#submit').on('click',function(){

    add_data();
  });
  $('#input_datefrom').datepicker({
    format: 'yyyy-mm-dd',
    todayBtn: true,
    language: 'th',             //เปลี่ยน label ต่างของ ปฏิทิน ให้เป็น ภาษาไทย   (ต้องใช้ไฟล์ bootstrap-datepicker.th.min.js นี้ด้วย)
    thaiyear: true              //Set เป็นปี พ.ศ.
  }).datepicker("setDate", "0");  //กำหนดเป็นวันปัจุบัน
  $('#input_senddate').datepicker({
    format: 'yyyy-mm-dd',
    todayBtn: true,
    language: 'th',             //เปลี่ยน label ต่างของ ปฏิทิน ให้เป็น ภาษาไทย   (ต้องใช้ไฟล์ bootstrap-datepicker.th.min.js นี้ด้วย)
    thaiyear: true              //Set เป็นปี พ.ศ.
  });
});
function add_data(){
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
    }
  });
  $.ajax({
    url: url,
    type: 'POST',
    dataType: 'JSON',
    data: {
      action:'add',
      doc: $('input[name=doc]').val(),
      doc_no: $('input[name=doc_no]').val(),
      datefrom: $('input[name=datefrom]').val(),
      from: $('input[name=from]').val(),
      to: $('input[name=to]').val(),
      subject: $('input[name=subject]').val(),
      budget: $('input[name=budget]').val(),
      recipence: $('input[name=recipence]').val(),
      sendto: $('input[name=sendto]').val(),
      senddate: $('input[name=senddate]').val(),
      note:$('input[name=note]').val()},
    })
    .done(function(result) {
      console.log(result);
      $('input[name=doc]').val(result['max']+1);
      get_data();
      alert("เพิ่มข้อมูลเสร็จสิ้น");
    })
    .fail(function(xhr, status, error) {
      //console.error(xhr, status, error.toString())
    })
    .always(function() {
      console.log("complete");
    });
  }
  function get_data(){
    table.clear().draw();
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      }
    });
    $.ajax({
      url: url,
      type: 'POST',
      dataType: 'JSON',
      data: {
        action:'all'},
      })
      .done(function(result) {
        console.log(result);
        $.each(result['data'], function(index, val) {
          result['data'][index]['button'] = '<button onclick="remove('+result['data'][index]['id']+');" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></button>';
        });
        var i = table.rows.add(result['data']).draw();
        table.rows(i).nodes().to$().attr("id", result['data']['id']);
        $('input[name=doc]').val(result['max']+1);
      })
      .fail(function(xhr, status, error) {
        console.error(xhr, status, error.toString());
      })
      .always(function() {
        console.log("complete");
      });
    }
    function remove(id){
      if(confirm("ยืนยันการลบข้อมูล")){
        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
          }
        });
        $.ajax({
          url: url,
          type: 'POST',
          dataType: 'JSON',
          data: {action: 'delete',
          id:id}
        })
        .done(function(result) {
          console.log("success");
          console.log(result);
          if(result['data']==true){
            alert('ลบเสร็จสิ้น');
          }
        })
        .fail(function(xhr, status, error) {
          console.log("error");
          console.error(xhr, status, error.toString());
        })
        .always(function() {
          console.log("complete");
          get_data();
        });
      }
    }
    </script>
