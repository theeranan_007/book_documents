<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookDocument extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('book_document', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('doc')->unsigned();
            $table->string('doc_no')->nullable(true);
            $table->date('datefrom')->nullable(true);
            $table->string('from')->nullable(true);
            $table->string('to')->nullable(true);
            $table->text('subject')->nullable(true);
            $table->float('budget')->nullable(true);
            $table->string('recipence')->nullable(true);
            $table->string('sendto')->nullable(true);
            $table->date('senddate')->nullable(true);
            $table->text('note')->nullable(true);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('book_document');
    }
}
