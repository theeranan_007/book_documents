<?php

namespace App\Http\Controllers;

use App\SearchModel;
use App\book_document;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        return view('search')->with("url", "search")->with("data", null);
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        //
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        //
        switch ($request->action) {
      case 'search':{
        $param=$request->all();
        $str=array();
        $i=0;
        foreach ($param as $key => $value) {
            if ($key!='action') {
                if ($value!=null) {
                    if ($key=="datefrom" || $key=="senddate") {
                        $value=date('Y-m-d', strtotime($value));
                    }
                    $str[$i] = [$key,'LIKE',$value.'%'];
                    $i++;
                }
            }
        }
        $response['data'] = book_document::where($str)->get();

        return response()->json($response);
      }
      break;
      case 'delete':{
        $response['data'] = book_document::find($request->id)->delete();
        return response()->json($response);
      }
      break;
      default:
      # code...
      break;
    }
    }

    /**
    * Display the specified resource.
    *
    * @param  \App\SearchModel  $searchModel
    * @return \Illuminate\Http\Response
    */
    public function show(SearchModel $searchModel)
    {
        //
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  \App\SearchModel  $searchModel
    * @return \Illuminate\Http\Response
    */
    public function edit(SearchModel $searchModel)
    {
        //
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \App\SearchModel  $searchModel
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, SearchModel $searchModel)
    {
        //
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  \App\SearchModel  $searchModel
    * @return \Illuminate\Http\Response
    */
    public function destroy(SearchModel $searchModel)
    {
        //
    }
}
