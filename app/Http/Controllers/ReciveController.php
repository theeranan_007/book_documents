<?php

namespace App\Http\Controllers;

use App\ReciveModel;
use App\book_document;
use Illuminate\Http\Request;

class ReciveController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $max = book_document::max('doc');
        $data = book_document::all();
        return view('recive')->with("url", "recive")->with('data', $data)->with('max', $max);
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        //
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        switch ($request->action) {
      case 'add':{
        if ($request->datefrom!=null) {
            $request->datefrom=date('Y-m-d', strtotime($request->datefrom));
        }

        if ($request->senddate!=null) {
            $request->datefrom = date('Y-m-d', strtotime($request->datefrom));
        }
        $response = array();
        $response['error'] = book_document::where('doc', $request->doc)->count();
        $response['data'] = book_document::all();
        if ($response['error']!=null) {
            return response()->json($response);
        } else {
            $insert = new book_document;
            $insert->doc = $request->doc;
            $insert->doc_no = $request->doc_no;
            $insert->datefrom = $request->datefrom;
            $insert->from = $request->from;
            $insert->to = $request->to;
            $insert->subject = $request->subject;
            $insert->budget = $request->budget;
            $insert->recipence = $request->recipence;
            $insert->sendto = $request->sendto;
            $insert->senddate = $request->senddate;
            $insert->note = $request->note;
            $insert->save();
            return response()->json($insert);
        }
        }
        break;
        case 'all':{
          $response['max'] = book_document::max('doc');
          $response['data'] = book_document::all();
          return response()->json($response);}
          break;
          case 'delete':{
            $response['data'] = book_document::find($request->id)->delete();
            return response()->json($response);
          }
          default:
          # code...
          break;
        }
    }

    /**
    * Display the specified resource.
    *
    * @param  \App\ReciveModel  $reciveModel
    * @return \Illuminate\Http\Response
    */
    public function show(ReciveModel $reciveModel)
    {
        //
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  \App\ReciveModel  $reciveModel
    * @return \Illuminate\Http\Response
    */
    public function edit(ReciveModel $reciveModel)
    {
        //
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \App\ReciveModel  $reciveModel
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, ReciveModel $reciveModel)
    {
        //
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  \App\ReciveModel  $reciveModel
    * @return \Illuminate\Http\Response
    */
    public function destroy(ReciveModel $reciveModel)
    {
        //
    }
}
