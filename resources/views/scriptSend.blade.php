<script>
var url = "{{action('SendController@store')}}";
var table = $('#datatable').DataTable({
  searching:false,
  "fnCreatedRow": function (nRow, aData, iDataIndex) {
    $(nRow).attr('id', aData["id"]);
  },
  "columns": [
    { "title": "เลขทะเบียนรับ","data": "doc" },
    { "title": "ที่","data": "doc_no" },
    { "title": "ลงวันที่","data": "datefrom" },
    { "title": "จาก","data": "from" },
    { "title": "ถึง","data": "to" },
    { "title": "เรื่อง","data": "subject" },
    { "title": "งบประมาณ","data": "budget" },
    { "title": "รับเรื่อง","data": "recipence" },
    { "title": "ส่งแล้ว","data": "sendto" },
    { "title": "ส่งวันที่","data": "senddate" },
    { "title": "หมายเหตุ","data": "note" },
    { "title": "#","data": "button" },
    { "title": "วันที่เข้า","data": "created_at" },
    { "title": "วันที่อัพเดท","data": "updated_at" }
  ]
});
$(document).ready(function(){

  $('input').keypress(function(event){

    if(event.which=='13'){
      search_data();
    }
  });
  $('#clear').on('click',function(){
    clear();
  });
  // $('#search').on('click',function(){
  //
  //   search_data();
  // });
  // $('#save').on('click',function(){
  //
  //   update_data();
  // });
  $('#input_datefrom').datepicker({
    format: 'yyyy-mm-dd',
    todayBtn: true,
    language: 'th',             //เปลี่ยน label ต่างของ ปฏิทิน ให้เป็น ภาษาไทย   (ต้องใช้ไฟล์ bootstrap-datepicker.th.min.js นี้ด้วย)
    thaiyear: true              //Set เป็นปี พ.ศ.
  });
  $('#input_senddate').datepicker({
    format: 'yyyy-mm-dd',
    todayBtn: true,
    language: 'th',             //เปลี่ยน label ต่างของ ปฏิทิน ให้เป็น ภาษาไทย   (ต้องใช้ไฟล์ bootstrap-datepicker.th.min.js นี้ด้วย)
    thaiyear: true              //Set เป็นปี พ.ศ.
  });
});
function clear(){
        $('input[name=id]').val("");
        $('input[name=doc]').val("");
        $('input[name=doc_no]').val("");
        $('input[name=datefrom]').val("");
        $('input[name=from]').val("");
        $('input[name=to]').val("");
        $('input[name=subject]').val("");
        $('input[name=budget]').val("");
        $('input[name=recipence]').val("");
        $('input[name=sendto]').val("");
        $('input[name=senddate]').val("");
        $('input[name=note]').val("");
}
 function search_data(){
   get_data();
//   $.ajaxSetup({
//     headers: {
//       'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
//     }
//   });
//   $.ajax({
//     url: url,
//     type: 'POST',
//     dataType: 'JSON',
//     data: {
//       action:'search',
//       doc: $('input[name=doc]').val(),
//       doc_no: $('input[name=doc_no]').val(),
//       datefrom: $('input[name=datefrom]').val(),
//       from: $('input[name=from]').val(),
//       to: $('input[name=to]').val(),
//       subject: $('input[name=subject]').val(),
//       budget: $('input[name=budget]').val(),
//       recipence: $('input[name=recipence]').val(),
//       sendto: $('input[name=sendto]').val(),
//       senddate: $('input[name=senddate]').val(),
//       note:$('input[name=note]').val()},
//     })
//     .done(function(result) {
//       console.log(result);
//       if(result['data'][0]['id']!=null){
//       $('input[name=id]').val(result['data'][0]['id']);
//       $('input[name=doc]').val(result['data'][0]['doc']);
//       $('input[name=doc_no]').val(result['data'][0]['doc_no']);
//       $('input[name=datefrom]').val(result['data'][0]['datefrom']);
//       $('input[name=from]').val(result['data'][0]['from']);
//       $('input[name=to]').val(result['data'][0]['to']);
//       $('input[name=subject]').val(result['data'][0]['subject']);
//       $('input[name=budget]').val(result['data'][0]['budget']);
//       $('input[name=recipence]').val(result['data'][0]['recipence']);
//       $('input[name=sendto]').val(result['data'][0]['sendto']);
//       $('input[name=senddate]').val(result['data'][0]['senddate']);
//       $('input[name=note]').val(result['data'][0]['note']);
//       get_data();
//     }
//     })
//     .fail(function(xhr, status, error) {
//       console.error(xhr, status, error.toString());
//     })
//     .always(function() {
//       console.log("complete");
//     });
 }
  function update_data(){
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      }
    });
    $.ajax({
      url: url,
      type: 'POST',
      dataType: 'JSON',
      data: {
        action:'update',
        doc: $('input[name=doc]').val(),
        doc_no: $('input[name=doc_no]').val(),
        datefrom: $('input[name=datefrom]').val(),
        from: $('input[name=from]').val(),
        to: $('input[name=to]').val(),
        subject: $('input[name=subject]').val(),
        budget: $('input[name=budget]').val(),
        recipence: $('input[name=recipence]').val(),
        sendto: $('input[name=sendto]').val(),
        senddate: $('input[name=senddate]').val(),
        note:$('input[name=note]').val()},
      })
      .done(function(result) {
        console.log(result);
        get_data();
        alert("อัพเดทเสร็จสิ้น");
      })
      .fail(function(xhr, status, error) {
        console.error(xhr, status, error.toString());
      })
      .always(function() {
        console.log("complete");
      });
    }
  function get_data(){
    table.clear().draw();
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      }
    });
    $.ajax({
      url: url,
      type: 'POST',
      dataType: 'JSON',
      data: {
        action:'search',
        doc: $('input[name=doc]').val(),
        doc_no: $('input[name=doc_no]').val(),
        datefrom: $('input[name=datefrom]').val(),
        from: $('input[name=from]').val(),
        to: $('input[name=to]').val(),
        subject: $('input[name=subject]').val(),
        budget: $('input[name=budget]').val(),
        recipence: $('input[name=recipence]').val(),
        sendto: $('input[name=sendto]').val(),
        senddate: $('input[name=senddate]').val(),
        note:$('input[name=note]').val()},
      })
      .done(function(result) {
        console.log(result);
        $.each(result['data'], function(index, val) {
          result['data'][index]['button'] = '<button onclick="remove('+result['data'][index]['id']+');" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></button>'
                  +'<button onclick="edit('+result['data'][index]['id']+');" class="btn btn-warning btn-sm"><i class="fa fa-edit"></i></button>';
        });
        var i = table.rows.add(result['data']).draw();
        table.rows(i).nodes().to$().attr("id", result['data']['id']);
      })
      .fail(function(xhr, status, error) {
        console.error(xhr, status, error.toString());
      })
      .always(function() {
        console.log("complete");
      });
    }
    function remove(id){
      if(confirm("ยืนยันการลบข้อมูล")){
        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
          }
        });
        $.ajax({
          url: url,
          type: 'POST',
          dataType: 'JSON',
          data: {action: 'delete',
          id:id}
        })
        .done(function(result) {
          console.log("success");
          console.log(result);
          if(result['data']==true){
            alert('ลบเสร็จสิ้น');
          }
        })
        .fail(function(xhr, status, error) {
          console.log("error");
          console.error(xhr, status, error.toString());
        })
        .always(function() {
          console.log("complete");
          get_data();
        });
      }
    }
    function edit(id){
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
      });
      $.ajax({
        url: url,
        type: 'POST',
        dataType: 'JSON',
        data: {
          action:'search_byid',
          id: id
        }
        })
        .done(function(result) {
          console.log(result);
          $('input[name=id]').val(result['data']['id']);
          $('input[name=doc]').val(result['data']['doc']);
          $('input[name=doc_no]').val(result['data']['doc_no']);
          $('input[name=datefrom]').val(result['data']['datefrom']);
          $('input[name=from]').val(result['data']['from']);
          $('input[name=to]').val(result['data']['to']);
          $('input[name=subject]').val(result['data']['subject']);
          $('input[name=budget]').val(result['data']['budget']);
          $('input[name=recipence]').val(result['data']['recipence']);
          $('input[name=sendto]').val(result['data']['sendto']);
          $('input[name=senddate]').val(result['data']['senddate']);
          $('input[name=note]').val(result['data']['note']);
          search_data();
        })
        .fail(function(xhr, status, error) {
          console.error(xhr, status, error.toString());
        })
        .always(function() {
          console.log("complete");
        });
    }
    </script>
