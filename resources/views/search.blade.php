@extends('app')
@section('content')
<div class="content-wrapper">


  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
    <!-- Default box -->

      <!-- <form class="" action="{{action('SearchController@store')}}" method="POST" > -->
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">ค้นหา</h3>

        <!--<div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                  title="Collapse">
            <i class="fa fa-minus"></i></button>
          <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
            <i class="fa fa-times"></i></button>
        </div>-->
      </div>
      <div class="box-body">
        <div class="row">
          <div class="col-md-1">
            <div class="form-group">
                      <label for="input_doc">เลขทะเบียนรับ</label>
                      <input type="number" class="form-control" id="input_doc" name="doc">
                    </div>
          </div>
          <div class="col-md-2">
            <div class="form-group">
              <label for="input_doc_no">ที่</label>
              <input type="text" class="form-control" id="input_doc_no" name="doc_no">
            </div>
          </div>
          <div class="col-md-1">
            <div class="form-group">
              <label for="input_datefrom">ลงวันที่</label>
              <input type="text" class="form-control" id="input_datefrom" name="datefrom">
            </div>
          </div>
          <div class="col-md-1">
            <div class="form-group">
              <label for="input_from">จาก</label>
              <input type="text" class="form-control" id="input_from" name="from">
            </div>
          </div>
          <div class="col-md-1">
            <div class="form-group">
              <label for="input_to">ถึง</label>
              <input type="text" class="form-control" id="input_to" name="to">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label for="input_subject">เรื่อง</label>
              <input type="text" class="form-control" id="input_subject" name="subject" >
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-3">
            <div class="form-group">
                      <label for="input_budget">งบประมาณ</label>
                      <input type="number" class="form-control" id="input_budget" name="budget">
                    </div>

          </div>
          <div class="col-md-1">
            <div class="form-group">
                      <label for="input_recipence">รับเรื่อง</label>
                      <input type="text" class="form-control" id="input_recipence" name="recipence">
                    </div>

      </div>
      <div class="col-md-1">
        <div class="form-group">
                  <label for="input_sendto">ส่งถึง</label>
                  <input type="text" class="form-control" id="input_sendto" name="sendto">
                </div>
    </div>
    <div class="col-md-1">
      <div class="form-group">
        <label for="input_senddate">ส่งวันที่</label>
        <input type="text" class="form-control" id="input_senddate" name="senddate">
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label for="input_note">หมายเหตุ</label>
        <input type="text" class="form-control" id="input_note" name="note" >
      </div>
    </div>


      </div>


      <!-- /.box-body -->
      <div class="box-footer">

        <button type="button" id="submit" class="btn btn-primary pull-right" ><i class="fa fa-search"> ค้นหา</i></button>
      </div>
      <!-- /.box-footer-->
    </div>
    <!-- /.box -->
  </div>

  <!-- </form> -->
</div>
</div>
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">รายการ</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body table-responsive">
            <table id="datatable" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>เลขทะเบียนรับ</th>
                  <th>ที่</th>
                  <th>ลงวันที่</th>
                  <th>จาก</th>
                  <th>ถึง</th>
                  <th>เรื่อง</th>
                  <th>งบประมาณ</th>
                  <th>รับเรื่อง</th>
                  <th>ส่งแล้ว</th>
                  <th>ส่งวันที่</th>
                  <th>หมายเหตุ</th>
                  <th>#</th>
                  <th>วันที่เข้า</th>
                  <th>วันที่อัพเดท</th>
                </tr>
                </thead>
                <tbody>
                  @if($data!=null)
                  @foreach ($data as $key => $value)
                    <tr>
                      <td>{{$value->doc}}</td>
                      <td>{{$value->doc_no}}</td>
                      <td>{{$value->datefrom}}</td>
                      <td>{{$value->from}}</td>
                      <td>{{$value->to}}</td>
                      <td>{{$value->subject}}</td>
                      <td>{{$value->budget}}</td>
                      <td>{{$value->recipence}}</td>
                      <td>{{$value->sendto}}</td>
                      <td>{{$value->senddate}}</td>
                      <td>{{$value->note}}</td>
                      <td><button onclick="remove({{$value->id}});" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></button></td>
                      <td>{{$value->created_at}}</td>
                      <td>{{$value->updated_at}}</td>
                    </tr>
                  @endforeach
                  @endif
                </tbody>
              </table>
      </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
    </div>
  </section>
  <!-- /.content -->
</div>
@stop
