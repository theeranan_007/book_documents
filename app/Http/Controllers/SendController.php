<?php

namespace App\Http\Controllers;

use App\ReciveModel;
use App\book_document;
use Illuminate\Http\Request;

class SendController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    $max = book_document::max('doc');
    $data = book_document::all();
    return view('send')->with("url", "send")->with('data', $data)->with('max', $max);
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    //
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request)
  {
    switch ($request->action) {
      case 'update':{
        if ($request->datefrom!=null) {
          $request->datefrom=date('Y-m-d', strtotime($request->datefrom));
        }

        if ($request->senddate!=null) {
          $request->datefrom = date('Y-m-d', strtotime($request->datefrom));
        }
        $response = array();
        $response['error'] = book_document::where('doc', $request->doc)->count();
        $response['data'] = book_document::all();
        if ($response['error']!=null) {
          return response()->json($response);
        } else {
          $update = book_document::find($request->id);
          $update->doc = $request->doc;
          $update->doc_no = $request->doc_no;
          $update->datefrom = $request->datefrom;
          $update->from = $request->from;
          $update->to = $request->to;
          $update->subject = $request->subject;
          $update->budget = $request->budget;
          $update->recipence = $request->recipence;
          $update->sendto = $request->sendto;
          $update->senddate = $request->senddate;
          $update->note = $request->note;
          $update->save();
          return response()->json($update);
        }
      }
      break;
      case 'search_byid':{
        $response['data'] = book_document::find($request->id)->first();
        return response()->json($response);
      }
      break;
      case 'search':{
        $param=$request->all();
        $str=array();
        $i=0;


        foreach ($param as $key => $value) {
          if ($key!='action') {
            if ($value!=null) {
              if ($key=="datefrom" || $key=="senddate") {
                $value=date('Y-m-d', strtotime($value));
              }
              $str[$i] = [$key,'LIKE',$value.'%'];
              $i++;
            }
          }
        }

        $response['data'] = book_document::where($str)->get();
        return response()->json($response);
      }
      break;
      case 'all':{
        $response['max'] = book_document::max('doc');
        $response['data'] = book_document::all();
        return response()->json($response);}
        break;
        case 'delete':{
          $response['data'] = book_document::find($request->id)->delete();
          return response()->json($response);
        }
        default:
        # code...
        break;
      }
    }

    /**
    * Display the specified resource.
    *
    * @param  \App\ReciveModel  $reciveModel
    * @return \Illuminate\Http\Response
    */
    public function show(ReciveModel $reciveModel)
    {
      //
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  \App\ReciveModel  $reciveModel
    * @return \Illuminate\Http\Response
    */
    public function edit(ReciveModel $reciveModel)
    {
      //
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \App\ReciveModel  $reciveModel
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, ReciveModel $reciveModel)
    {
      //
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  \App\ReciveModel  $reciveModel
    * @return \Illuminate\Http\Response
    */
    public function destroy(ReciveModel $reciveModel)
    {
      //
    }
  }
